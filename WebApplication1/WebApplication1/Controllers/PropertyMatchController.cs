﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PropertyMatchController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<PropertyMatchController> _logger;

        public PropertyMatchController(ILogger<PropertyMatchController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public bool Get(string key)
        {
  /*          Only The Best Real Estate!(AgencyCode: OTBRE)
Only The Best Real Estate! use a lot of punctuation in their property names and addresses.
A property is a match if both the property name and address match when punctuation is
excluded.
For example, the property “*Super * -High! APARTMENTS(Sydney)” located at “32 Sir
John - Young Crescent, Sydney, NSW.” would be a match for a property called “Super High
  Apartments, Sydney” located at “32 Sir John Young Crescent, Sydney NSW”. */

            var agencyProp = new LotsOfPunctuationFilter(("OTBRE", "Only The Best Real Estate!"),"*Super * -High! APARTMENTS(Sydney)" , "32 Sir John - Young Crescent, Sydney, NSW." );

            return agencyProp.IsMatch(agencyProp, new Property(("OTBRE", "Only The Best Real Estate!"))
             {
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John Young Crescent, Sydney NSW"
            });
            
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WebApplication1
{
    public class Property : IPropertyMatcher
    {
        public Property((string, string) code)
        {
            AgencyCode = code;
        }

        public string Address { get; set; }

        public (string, string) AgencyCode { get; }

        public string Name { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        virtual public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            throw new NotImplementedException();
        }
    }
    public class LotsOfPunctuationFilter : Property
    {
        public LotsOfPunctuationFilter((string, string) code, string name, string address):base(code)
        {
            Name = Regex.Replace(name, "[^a-zA-Z0-9]", "");
            Address = Regex.Replace(address, "[^a-zA-Z0-9]", "");

        }
        
        public override bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            
            if (string.IsNullOrEmpty(this.Name) && string.IsNullOrEmpty(this.Address))
                return false;
            return (agencyProperty.Name.Contains(Regex.Replace(databaseProperty.Name, "[^a-zA-Z0-9]", ""), StringComparison.OrdinalIgnoreCase) || agencyProperty.Address.Contains(Regex.Replace(databaseProperty.Address, "[^a-zA-Z0-9]", ""), StringComparison.OrdinalIgnoreCase));
                  
        }
    }
}
